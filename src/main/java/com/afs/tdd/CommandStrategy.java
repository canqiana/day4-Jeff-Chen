package com.afs.tdd;

public interface CommandStrategy {
    void runCommand(Location location);
}
