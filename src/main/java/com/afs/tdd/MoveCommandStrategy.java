package com.afs.tdd;

public class MoveCommandStrategy implements CommandStrategy{

    @Override
    public void runCommand(Location location) {
        int coordinateY = location.getCoordinateY();
        int coordinateX = location.getCoordinateX();

        switch (location.getDirection()){
            case North:
                location.setCoordinateY(coordinateY + 1); break;
            case West:
                location.setCoordinateX(coordinateX - 1); break;
            case South:
                location.setCoordinateY(coordinateY - 1); break;
            case East:
                location.setCoordinateX(coordinateX + 1); break;
        }
    }
}
