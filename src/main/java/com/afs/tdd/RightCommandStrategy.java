package com.afs.tdd;

public class RightCommandStrategy implements CommandStrategy{
    @Override
    public void runCommand(Location location) {
        switch (location.getDirection()){
            case North:
                location.setDirection(Direction.East); break;
            case West:
                location.setDirection(Direction.North); break;
            case South:
                location.setDirection(Direction.West); break;
            case East:
                location.setDirection(Direction.South); break;
        }
    }
}
