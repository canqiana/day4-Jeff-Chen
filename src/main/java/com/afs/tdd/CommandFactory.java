package com.afs.tdd;

public class CommandFactory {
    private CommandStrategy commandStrategy;
    public void generateCommand(Command command){
        switch (command){
            case Move: commandStrategy =  new MoveCommandStrategy(); break;
            case Left: commandStrategy = new LeftCommandStrategy(); break;
            case Right: commandStrategy = new RightCommandStrategy();
        }
    }

    public CommandStrategy getCommandStrategy(){
        return commandStrategy;
    }
}
