package com.afs.tdd;

public class LeftCommandStrategy implements CommandStrategy{
    @Override
    public void runCommand(Location location) {
        switch (location.getDirection()){
            case North:
                location.setDirection(Direction.West); break;
            case West:
                location.setDirection(Direction.South); break;
            case South:
                location.setDirection(Direction.East); break;
            case East:
                location.setDirection(Direction.North); break;
        }
    }

}
