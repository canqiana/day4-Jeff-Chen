package com.afs.tdd;

import java.util.List;

public class MarsRover {

    private final Location location;

    public Location getLocation() {
        return location;
    }

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeBatchCommand(List<Command> commands) {
        CommandFactory commandFactory = new CommandFactory();
        commands.forEach(command -> {
            commandFactory.generateCommand(command);
            CommandStrategy commandStrategy = commandFactory.getCommandStrategy();
            commandStrategy.runCommand(this.location);
        });

    }
}
