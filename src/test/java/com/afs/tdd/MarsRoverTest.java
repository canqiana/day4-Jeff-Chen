package com.afs.tdd;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarsRoverTest {
    //*************************************       0_0_North       *************************************************
    @Test
    void should_change_location_to_0_1_North_when_executeBatchCommand_given_location_0_0_North_and_command_Move() {
    //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
    //when
        marsRover.executeBatchCommand(List.of(Command.Move));
    //then
        assertEquals(0,marsRover.getLocation().getCoordinateX());
        assertEquals(1,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_West_when_executeBatchCommand_given_location_0_0_North_and_command_Left() {
    //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
    //when
        marsRover.executeBatchCommand(List.of(Command.Left));
    //then
        assertEquals(0,marsRover.getLocation().getCoordinateX());
        assertEquals(0,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.West,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_East_when_executeBatchCommand_given_location_0_0_North_and_command_Right() {
    //given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
    //when
        marsRover.executeBatchCommand(List.of(Command.Right));
    //then
        assertEquals(0,marsRover.getLocation().getCoordinateX());
        assertEquals(0,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East,marsRover.getLocation().getDirection());
    }

    //*************************************       0_0_West       *************************************************
    @Test
    void should_change_location_to_minusOne_0_West_when_executeBatchCommand_given_location_0_0_West_and_command_Move() {
    //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
    //when
        marsRover.executeBatchCommand(List.of(Command.Move));
    //then
        assertEquals(-1,marsRover.getLocation().getCoordinateX());
        assertEquals(0,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.West,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_South_when_executeBatchCommand_given_location_0_0_West_and_command_Left() {
    //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
    //when
        marsRover.executeBatchCommand(List.of(Command.Left));
    //then
        assertEquals(0,marsRover.getLocation().getCoordinateX());
        assertEquals(0,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.South,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_North_when_executeBatchCommand_given_location_0_0_West_and_command_Right() {
    //given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);
     //when
        marsRover.executeBatchCommand(List.of(Command.Right));
     //then
        assertEquals(0,marsRover.getLocation().getCoordinateX());
        assertEquals(0,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North,marsRover.getLocation().getDirection());
    }

    //*************************************       0_0_South       ************************************************
    @Test
    void should_change_location_to_0_minusOne_South_when_executeBatchCommand_given_location_0_0_South_and_command_Move() {
    //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
     //when
        marsRover.executeBatchCommand(List.of(Command.Move));
     //then
        assertEquals(0,marsRover.getLocation().getCoordinateX());
        assertEquals(-1,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.South,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_East_when_executeBatchCommand_given_location_0_0_South_and_command_Left() {
    //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
     //when
        marsRover.executeBatchCommand(List.of(Command.Left));
     //then
        assertEquals(0,marsRover.getLocation().getCoordinateX());
        assertEquals(0,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_West_when_executeBatchCommand_given_location_0_0_South_and_command_Right() {
    //given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);
     //when
        marsRover.executeBatchCommand(List.of(Command.Right));
     //then
        assertEquals(0,marsRover.getLocation().getCoordinateX());
        assertEquals(0,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.West,marsRover.getLocation().getDirection());
    }

    //*************************************       0_0_East       ************************************************
    @Test
    void should_change_location_to_1_0_East_when_executeBatchCommand_given_location_0_0_East_and_command_Move() {
    //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
     //when
        marsRover.executeBatchCommand(List.of(Command.Move));
     //then
        assertEquals(1,marsRover.getLocation().getCoordinateX());
        assertEquals(0,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_North_when_executeBatchCommand_given_location_0_0_East_and_command_Left() {
    //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
     //when
        marsRover.executeBatchCommand(List.of(Command.Left));
     //then
        assertEquals(0,marsRover.getLocation().getCoordinateX());
        assertEquals(0,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North,marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_location_to_0_0_South_when_executeBatchCommand_given_location_0_0_East_and_command_Right() {
    //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
     //when
        marsRover.executeBatchCommand(List.of(Command.Right));
     //then
        assertEquals(0,marsRover.getLocation().getCoordinateX());
        assertEquals(0,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.South,marsRover.getLocation().getDirection());
    }

    //*************************************       0_0_East       ************************************************
    @Test
    void should_change_location_to_2_1_East_when_executeBatchCommand_given_location_0_0_East_and_command_MLMRM() {
    //given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);
     //when
        marsRover.executeBatchCommand(List.of(Command.Move,Command.Left,Command.Move,Command.Right,Command.Move));
     //then
        assertEquals(2,marsRover.getLocation().getCoordinateX());
        assertEquals(1,marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East,marsRover.getLocation().getDirection());
    }
}
