Objective: 

1. Code Review : review each other's homework with my teammates , find a bug in introduce method and fix it. Finally I showcase the code.
1. Learn what is Unit Testing and understand that it consists of four elements: Test description, Given/prepare, When/execute, Then/verify. At last, practice it with Expense-Service example.
1. Learn what is TDD and understand that it consists of three step:  Write a failed test, Write code to make failed test pass, Refactor. At last, practice it with Fizz-Buzz and Mars-Rover examples.

Reflective:  Fruitful.

Interpretive:  I learn a lot of new knowledge in this day's study, such as Unit Testing and TDD. 

Decisional:  I will practice Java more in the future. I will also actively seek the help of teachers and classmates when I have problem.